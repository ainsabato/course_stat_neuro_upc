{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: Introduction to Brain Connectivity\n",
    "### by Gorka Zamora-López, Ph.D.\n",
    "\n",
    "XII Summer School in Statistics and Operation Research, July 2-6, 2018.   \n",
    "Universitat Politècnica de Catalunya, Barcelona, Spain.\n",
    "\n",
    "#### Dependencies\n",
    "This tutorial requires a Python 3.X installation together with NumPy and Matplotlib libraries. To characterise the networks we will use the *[Graph Analysis Library](https://github.com/gorkazl/pyGAlib)* (**GAlib**), developed by Gorka Zamora-López. Find the library and installation instructions in its GitHub repository.   \n",
    "**NOTE** The official version of **GAlib** is released for Python 2.7. I will provide the newer version for Python 3.X to the students during the course. Find it in the BitBucket repository, in the \"Code\" folder.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import fundamental libraries\n",
    "\n",
    "Let's get started. For that, the very first thing we need is to load the fundamental libraries we need to work. We will make an absolut import of *NumPy*.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timeit import default_timer as timer\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from numpy import*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structural Connectivity –––––––––––––––––––––––––––––––––––––––––––––––\n",
    "### Analysis of anatomical connectome of the cat\n",
    "#### Load the data and explore basic features of the network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataroot = 'datasets/resting/'\n",
    "catnet = loadtxt(dataroot + 'Cat/' + 'Cat53_cortex.txt', dtype=float)\n",
    "\n",
    "plt.figure()\n",
    "plt.imshow(catnet, cmap='gray_r')\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most graph measures exist for directed networks, which provides richer information. Check for example the `Reciprocity()` and `ReciprocalDegree()` functions. GAlib functions usually come with an optional parameter `directed` in order to specify whether the undirected or the directed version of the measure is desired. By default, the parameter is set for exploration of undirected graphs: `directed=False`.\n",
    "\n",
    "**For simplicity**, along the rest of the tutorial we will consider that long-range connectome of the cat is **symmetric**, which is a fairly good approximation. And we will ignore the weights of the links."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "netsym = 0.5 * (catnet + catnet.T)\n",
    "netsym = where(netsym,1,0).astype(uint8)\n",
    "\n",
    "plt.figure()\n",
    "plt.imshow(netsym, cmap='gray_r')\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fundamental properties of the network\n",
    "N = len(netsym)\n",
    "L = 0.5 * netsym.astype(bool).sum()\n",
    "Lmax = 0.5 * N*(N-1)\n",
    "dens = L / Lmax\n",
    "\n",
    "print('N: %d\\t\\tL: %d\\t\\tdensity: %1.3f' %(N, L, dens))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Local network properties (Exercise – 1)\n",
    "Let's start analysing the network. We will:\n",
    "1. find its degree distribution, and\n",
    "2. calculate the clustering coefficient\n",
    "\n",
    "First of all, we need to import the functions from the Graph Analysis Library (GAlib). For now, we will use the module *galib.py*, which contains functions to estimate graph measures out of a graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from galib import Degree, Clustering\n",
    "\n",
    "deg = Degree(netsym, directed=False)\n",
    "print('Degrees: Min = %d, Max = %d, Mean degree: %1.3f' %(deg.min(), deg.max(), deg.mean()))\n",
    "\n",
    "plt.figure()\n",
    "plt.hist(deg, bins=8, range=(0,40), rwidth=0.8, density=True)\n",
    "plt.xlabel('Node degree', fontsize=14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "C, Cnodes = Clustering(netsym, checkdirected=False)\n",
    "\n",
    "print( 'Clustering coefficient: %1.3f' %C )\n",
    "print( 'Local clustering: Min = %1.3f, Max = %1.3f, Average = %1.3f' %(Cnodes.min(), Cnodes.max(), Cnodes.mean()) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global network properties (Exercise – 2)\n",
    "\n",
    "1. Compute the diameter of the network and its average pathlength\n",
    "2. Calculate the betwenness centrality of the nodes\n",
    "\n",
    "The Dijkstra is a famous algorithm to find the graph distance between a pair of nodes in a graph. However, if we want to calculate the distance between all pairs of graphs, the Floyd-Warshall algorithm is faster than running Dijkstra N^2 times. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from galib import FloydWarshall\n",
    "Dij = FloydWarshall(netsym)\n",
    "diam = Dij.max()\n",
    "avpathlen = (Dij.sum() - Dij.trace()) / (N*(N-1))\n",
    "\n",
    "print('Diameter: %d\\tAv. Pathlen: %1.3f' %(diam, avpathlen))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from galib import PathsAllinOne\n",
    "Dij, bcnodes, paths, cycles = PathsAllinOne(netsym)\n",
    "print( '\\nBetweenness centrality. Min: %d\\tMax: %d' %(bcnodes.min(), bcnodes.max()) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Normalise BC of the nodes by all the existing shortest paths\n",
    "# diameter was diam=3\n",
    "npaths = len(paths[1]) + len(paths[2]) + len(paths[3])\n",
    "bcnormed = bcnodes / npaths \n",
    "\n",
    "print( 'Total number of shortest paths: %d' %npaths )\n",
    "print( 'Betweenness centrality. Min: %1.3f\\tMax: %1.3f' %(bcnormed.min(), bcnormed.max()) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.scatter(deg, bcnormed)\n",
    "plt.xlabel('Node degree', fontsize=14)\n",
    "plt.ylabel('Betweenness centrality', fontsize=14)\n",
    "plt.xlim(0,45)\n",
    "plt.grid(lw=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graph models. Null-models and benchmarks (Exercise – 3)\n",
    "\n",
    "We will now compare the cat connectome with typical reference networks: *random graphs* and *degree-preserving random graphs*. These comparisons are **not** to be regarded as actual **significance tests**; but they serve to understand where some of the properties of the real connectome may arise from.\n",
    "\n",
    "**Random graphs** are the fundamental (1st order) reference network model because the outcome of ALL graph measures strongly depends on how sparse, or how dense is the network. Thus, the properties of random graphs of same size *N* and number of links *L* as the real network indicate how far do the properties of the real network depend on their density.\n",
    "\n",
    "Beyond *N* and *L*, we often want to know how much of the observed properties of a network may depend also on a third constraint, namely, the degree-distribution. Specially because the presence of hubs may alter the properties of the network. For that, we need to generate reference graphs with the same *N*, *L* and **k-sequence** as the original connectome, which are maximally random under those constraints.\n",
    "\n",
    "In the GAlib library, the functions to generate different network models are located in the *gamodels.py* module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate ensemble of random graphs with same N and L as the cat connectome\n",
    "from gamodels import RandomGraph\n",
    "nrealiz = 10\n",
    "randnets = zeros((nrealiz,N,N), uint8)\n",
    "for re in range(nrealiz):\n",
    "    randnets[re] = RandomGraph(N,L, directed=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate ensemble of random graphs with same N and L and k-sequence as the cat connectome\n",
    "from gamodels import RewireNetwork\n",
    "rewnets = zeros((nrealiz,N,N), uint8)\n",
    "for re in range(nrealiz):\n",
    "    rewnets[re] = RewireNetwork(netsym, prewire=10, directed=False, weighted=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compare the degree distributions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deglist_rand = []\n",
    "deglist_rew = []\n",
    "for re in range(nrealiz):\n",
    "    deglist_rand += Degree(randnets[re], directed=False).tolist()\n",
    "    deglist_rew += Degree(rewnets[re], directed=False).tolist()\n",
    "deglist_rand = array(deglist_rand, dtype=uint)\n",
    "deglist_rew = array(deglist_rew, dtype=uint)\n",
    "\n",
    "print('\\nDEGREES')\n",
    "print('Cat \\t Min = %d, Max = %d,\\t Mean degree: %1.3f' %(deg.min(), deg.max(), deg.mean()))\n",
    "print('Random \\t Min = %d, Max = %d,\\t Mean degree: %1.3f' %(deglist_rand.min(), deglist_rand.max(), deglist_rand.mean()) )\n",
    "print('Rew \\t Min = %d, Max = %d,\\t Mean degree: %1.3f' %(deglist_rew.min(), deglist_rew.max(), deglist_rew.mean()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the degree distributions\n",
    "plt.figure()\n",
    "plt.hist(deglist_rand, bins=8, range=(0,40), rwidth=0.8, density=True)\n",
    "plt.xlabel('Node degree', fontsize=14)\n",
    "\n",
    "plt.figure()\n",
    "plt.hist(deglist_rew, bins=8, range=(0,40), rwidth=0.8, density=True)\n",
    "plt.xlabel('Node degree', fontsize=14)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compare the clustering coefficient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Clist_rand = zeros(nrealiz, float)\n",
    "Clist_rew = zeros(nrealiz, float)\n",
    "for re in range(nrealiz):\n",
    "    Crand, Cnodes_rand = Clustering(randnets[re])\n",
    "    Clist_rand[re] = Crand\n",
    "    Crew, Cnodes_rew = Clustering(rewnets[re])\n",
    "    Clist_rew[re] = Crew\n",
    "\n",
    "print('\\nCLUSTERING COEFFICIENT')\n",
    "print('Cat:\\t%1.3f \\nRandom:\\t%1.3f \\nRew:\\t%1.3f' %(C, Crand, Crew))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compare the pathlength and betweenness centrality"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, calculate pathlengths and BC from the RANDOM graphs\n",
    "avlenlist_rand = zeros(nrealiz, float)\n",
    "bclist_rand = []\n",
    "triuidx = triu_indices(N, k=1)\n",
    "for re in range(nrealiz):\n",
    "    Dij_rand, BC_rand, paths, cycles = PathsAllinOne(randnets[re])\n",
    "    values = Dij_rand[triuidx]\n",
    "    avlenlist_rand[re] = values.mean()\n",
    "    bclist_rand += BC_rand.tolist()\n",
    "\n",
    "# Calculate pathlengths and BC from the REWIRED graphs\n",
    "avlenlist_rew = zeros(nrealiz, float)\n",
    "bclist_rew = []\n",
    "for re in range(nrealiz):\n",
    "    Dij_rew, BC_rew, paths, cycles = PathsAllinOne(rewnets[re])\n",
    "    values = Dij_rew[triuidx]\n",
    "    avlenlist_rew[re] = values.mean()\n",
    "    bclist_rew += BC_rew.tolist()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Results for the average pathlength \n",
    "plt.figure()\n",
    "plt.title('Comparison of average pathlengths', fontsize=14)\n",
    "plt.bar( (1,2,3), (avpathlen, avlenlist_rand.mean(), avlenlist_rew.mean()) )\n",
    "plt.ylim(1,2)\n",
    "plt.xticks((1,2,3), ('Cat', 'Random', 'Rewired') )\n",
    "plt.grid(axis='y', lw=0.5)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Results for the betweenness centrality\n",
    "plt.figure()\n",
    "plt.scatter(deg, bcnodes)\n",
    "plt.plot(deglist_rand, bclist_rand, '.', color='red', zorder=10)\n",
    "#plt.plot(deglist_rew, bclist_rew, '.', color='orange', zorder=5)\n",
    "plt.xlabel('Node degree', fontsize=14)\n",
    "plt.ylabel('Betweenness centrality', fontsize=14)\n",
    "#plt.xlim(0,45)\n",
    "plt.grid(lw=0.5)\n",
    "\n",
    "# Try again with k**2. (comment'xlim' out). What happens?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functional Connectivity –––––––––––––––––––––––––––––––––––––––––––––––\n",
    "\n",
    "Usually, functional connectivity is obtain from dynamic / statistic measures to estimate the \"closeness\" between the temporal activity of two brain regions. Here, we don't have fMRI or EEG data from the brain of cats so, just for illustration purposes, we will compute an estimate (a prediction) of the FC. This estimate, *Topological similarity*, represents a diffusion process in the network. See Bettinardi, Deco et al. (2017) \"*[How structure sculpts function: Unveiling the contribution of anatomical connectivity to the brain's spontaneous correlation structure](http://aip.scitation.org/doi/10.1063/1.4980099)*\" Chaos, 27, 047409."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.linalg\n",
    "def TopologicalSimilarity(adjmatrix, coupling):\n",
    "    \"\"\"Computes the expected cross-correlation matrix of a given network.\n",
    "\n",
    "    This function estimates the expected correlations between its nodes that\n",
    "    a dynamical process on the network tends to generate. The estimation assumes\n",
    "    that the \"influence\" of one node over another distributes over all possible\n",
    "    paths but that the influence decays along the paths. This decay is estimated\n",
    "    as the communicability matrix. Pairwise correlation is assumed, according\n",
    "    to the patterns of inputs two nodes tend to receive.\n",
    "    See further details in our paper:\n",
    "    \"Bettinardi et al. Chaos 27:047409 (2017)\"\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    adjmatrix : ndarray of rank-2\n",
    "        The adjacency matrix of the network.\n",
    "    coupling : real valued number\n",
    "        The coupling strength of all connections.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    corrmatrix : ndarray of rank-2, same size as adjmatrix.\n",
    "        The correlation matrix of the expected functional connectivity.\n",
    "\n",
    "    See Also\n",
    "    --------\n",
    "    ExponentialMapping : Expected cross-correlation matrix of a given network.\n",
    "    CovarianceLinearGaussian : Covariance matrix of an Ornstein-Uhlenbeck process.\n",
    "    \"\"\"\n",
    "    # 0) Security check on the dtype\n",
    "    if adjmatrix.dtype not in ['float32','float64','float']:\n",
    "        adjmatrix = adjmatrix.astype(float64)\n",
    "\n",
    "    # 1) Compute the communicability matrix\n",
    "    Cmatrix = scipy.linalg.expm(coupling*adjmatrix).T\n",
    "\n",
    "    # 2) Compute the product between all columns (Cmatrix was transposed)\n",
    "    #    and the normalization\n",
    "    corrmatrix = inner(Cmatrix, Cmatrix)\n",
    "    norms = scipy.linalg.norm(Cmatrix, axis=1)\n",
    "    normmatrix = outer(norms, norms.T)\n",
    "\n",
    "    corrmatrix = corrmatrix / normmatrix\n",
    "    return corrmatrix\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Coupling strength\n",
    "g = 0.09 #0.0766\n",
    "fcnet = TopologicalSimilarity(netsym, g)\n",
    "\n",
    "plt.figure()\n",
    "plt.imshow(fcnet, cmap='jet')\n",
    "plt.clim(0,1)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from galib import Intensity\n",
    "intens = Intensity(fcnet, directed=False)\n",
    "\n",
    "# Compare SC degree and FC intensity\n",
    "plt.figure()\n",
    "plt.scatter(deg, intens)\n",
    "plt.xlabel('Node degree (SC)', fontsize=14)\n",
    "plt.ylabel('Node intensity (FC)', fontsize=14)\n",
    "plt.grid(lw=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the graph distance and the pair-wise correlations\n",
    "distvalues = Dij[triuidx]\n",
    "fcvalues = fcnet[triuidx]\n",
    "plt.figure()\n",
    "plt.plot(distvalues, fcvalues, '.')\n",
    "plt.xlabel('Graph distance (SC)', fontsize=14)\n",
    "plt.ylabel('Correlation (FC)', fontsize=14)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
