"""In this script we will perform the same steps as in the python notebook for
the Tutorial 'Introduction to Brain Connectivity'. Here, we will write the code
organised properly, as one should write it in a script.
"""

# Import python standard libraries
from timeit import default_timer as timer
import sys, os
# Import third party libraries

# Import your own libraries


# Define functions Here



################################################################################
# 0) PREPARE AND LOAD THE DATA

# 0.1) Symmetrise the networks


# 1) ANALYSIS OF STRUCTURAL CONNECTOME OF THE CAT ______________________________
# 1.0) Basic properties of the network


# 1.1) The degree distribution


# 1.2) Clustering coefficient


# 1.3) Compute the average pathlength and diameter
# Find the graph distance between all pairs of nodes first


# 1.4) Betweenness Centrality of the nodes



# 2) COMPARISON WITH REFERENCE NETWORKS
print('\nCOMPARISON WITH REFERENCE NETWORKS')
# 2.1) Generate ensemble of random graphs with same N and L


# 2.2) Generate ensemble of random graphs with same N and L and k-sequence


# 2.3) Compare the degree distributions


# Plot the degree distributions


# 2.4) Compare the clustering coefficients


# 2.5) Compare the pathlengths and the betweenness centralities
# First, calculate pathlengths and BC from the RANDOM graphs

# Calculate pathlengths and BC from the REWIRED graphs


# Results for the average pathlength


# Results for the betweenness centrality



# 3) ANALYSIS OF FUNCTIONAL CONNECTOME OF THE CAT ______________________________
# 3.1) Compute theoretical expected functional connectivity


# 3.2) Compare SC degree and FC intensity


# 3.3) Compare the graph distance and the pair-wise correlations


# Launch all the plots, ONLY at the end!!
plt.show()

#
