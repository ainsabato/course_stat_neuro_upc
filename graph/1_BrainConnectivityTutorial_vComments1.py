"""In this script we will perform the same steps as in the python notebook for
the Tutorial 'Introduction to Brain Connectivity'. Here, we will write the code
organised properly, as one should write it in a script.
"""

# Import python standard libraries
from timeit import default_timer as timer
import sys, os
# Import third party libraries

# Import your own libraries


# Define functions Here



################################################################################
# 0) PREPARE AND LOAD THE DATA



# 1) ANALYSIS OF STRUCTURAL CONNECTOME OF THE CAT ______________________________



# 2) COMPARISON WITH REFERENCE NETWORKS




# 3) ANALYSIS OF FUNCTIONAL CONNECTOME OF THE CAT ______________________________




# Launch all the plots, ONLY at the end!!
plt.show()

#
