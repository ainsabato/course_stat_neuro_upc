"""In this script we will perform the same steps as in the python notebook for
the Tutorial 'Introduction to Brain Connectivity'. Here, we will write the code
organised properly, as one should write it in a script.
"""

# Import python standard libraries
from timeit import default_timer as timer
# Import third party libraries
import matplotlib.pyplot as plt
from numpy import*
import scipy.linalg
# Import your own libraries
from galib import Degree, Clustering, FloydWarshall, PathsAllinOne, Intensity
from gamodels import RandomGraph, RewireNetwork


# Define functions Here
def TopologicalSimilarity(adjmatrix, coupling):
    """Computes the expected cross-correlation matrix of a given network.

    This function estimates the expected correlations between its nodes that
    a dynamical process on the network tends to generate. The estimation assumes
    that the "influence" of one node over another distributes over all possible
    paths but that the influence decays along the paths. This decay is estimated
    as the communicability matrix. Pairwise correlation is assumed, according
    to the patterns of inputs two nodes tend to receive.
    See further details in our paper:
    "Bettinardi et al. Chaos 27:047409 (2017)"

    Parameters
    ----------
    adjmatrix : ndarray of rank-2
        The adjacency matrix of the network.
    coupling : real valued number
        The coupling strength of all connections.

    Returns
    -------
    corrmatrix : ndarray of rank-2, same size as adjmatrix.
        The correlation matrix of the expected functional connectivity.

    See Also
    --------
    ExponentialMapping : Expected cross-correlation matrix of a given network.
    CovarianceLinearGaussian : Covariance matrix of an Ornstein-Uhlenbeck process.
    """
    # 0) Security check on the dtype
    if adjmatrix.dtype not in ['float32','float64','float']:
        adjmatrix = adjmatrix.astype(float64)

    # 1) Compute the communicability matrix
    Cmatrix = scipy.linalg.expm(coupling*adjmatrix).T

    # 2) Compute the product between all columns (Cmatrix was transposed)
    #    and the normalization
    corrmatrix = inner(Cmatrix, Cmatrix)
    norms = scipy.linalg.norm(Cmatrix, axis=1)
    normmatrix = outer(norms, norms.T)

    corrmatrix = corrmatrix / normmatrix
    return corrmatrix


################################################################################
# 0) PREPARE AND LOAD THE DATA
dataroot = 'datasets/resting/'
catnet = loadtxt(dataroot + 'Cat/' + 'Cat53_cortex.txt', dtype=float)

plt.figure()
plt.imshow(catnet, cmap='gray_r')
plt.colorbar()

# 0.1) Symmetrise the networks
netsym = 0.5 * (catnet + catnet.T)

plt.figure()
plt.imshow(netsym, cmap='gray_r')
plt.colorbar()


# 1) ANALYSIS OF STRUCTURAL CONNECTOME OF THE CAT ______________________________
# 1.0) Basic properties of the network
N = len(netsym)
L = 0.5 * netsym.astype(bool).sum()
Lmax = 0.5 * N*(N-1)
dens = L / Lmax

print('N: %d\t\tL: %d\t\tdensity: %1.3f' %(N, L, dens))

# 1.1) The degree distribution
deg = Degree(netsym, directed=False)

print('Degrees: Min = %d, Max = %d, Mean degree: %1.3f' \
                                        %(deg.min(), deg.max(), deg.mean()))

# Plot the k-distribution
plt.figure()
plt.hist(deg, bins=8, range=(0,40), rwidth=0.8)
plt.xlabel('Node degree', fontsize=14)

# 1.2) Clustering coefficient
C, Cnodes = Clustering(netsym, checkdirected=False)

print('\nClustering coefficient: %1.3f' %C)
print('Local clustering: Min = %1.3f, Max = %1.3f, Average = %1.3f' \
%(Cnodes.min(), Cnodes.max(), Cnodes.mean()) )

# 1.3) Compute the average pathlength and diameter
# Find the graph distance between all pairs of nodes first
Dij = FloydWarshall(netsym)
diam = Dij.max()
avpathlen = ( Dij.sum() - Dij.trace() ) / (N*(N-1))

print('Diameter: %d\tAv. Pathlen: %1.3f' %(diam, avpathlen))

# 1.4) Betweenness Centrality of the nodes
Dij, bcnodes, paths, cycles = PathsAllinOne(netsym)
print( '\nBetweenness centrality. Min: %d\tMax: %d' %(bcnodes.min(), bcnodes.max()) )

# Normalise the betweenness. Diameter was diam=3
npaths = len(paths[1]) + len(paths[2]) + len(paths[3])
bcnormed = bcnodes / npaths

print( 'Total number of shortest paths: %d' %npaths )
print( 'Betweenness centrality. Min: %1.3f\tMax: %1.3f' %(bcnormed.min(), bcnormed.max()) )

# Plot the relation between BC of the nodes and their degrees
plt.figure()
plt.scatter(deg, bcnormed)
plt.xlabel('Node degree', fontsize=14)
plt.ylabel('Betweenness centrality', fontsize=14)
plt.xlim(0,45)
plt.grid(lw=0.5)


# 2) COMPARISON WITH REFERENCE NETWORKS
print('\nCOMPARISON WITH REFERENCE NETWORKS')
# 2.1) Generate ensemble of random graphs with same N and L
nrealiz = 10
randnets = zeros((nrealiz,N,N), uint8)
for re in range(nrealiz):
    randnets[re] = RandomGraph(N,L, directed=False)

# 2.2) Generate ensemble of random graphs with same N and L and k-sequence
rewnets = zeros((nrealiz,N,N), uint8)
for re in range(nrealiz):
    rewnets[re] = RewireNetwork(netsym, prewire=10, directed=False, weighted=False)

# 2.3) Compare the degree distributions
# Compute degrees of all random and rewired networks
deglist_rand = []
deglist_rew = []
for re in range(nrealiz):
    deglist_rand += Degree(randnets[re], directed=False).tolist()
    deglist_rew += Degree(rewnets[re], directed=False).tolist()
deglist_rand = array(deglist_rand, dtype=uint)
deglist_rew = array(deglist_rew, dtype=uint)

print('\nCompare degrees ...')
print('Cat \t Min = %d, Max = %d, Mean degree: %1.3f' %(deg.min(), deg.max(), deg.mean()))
print('Random \t Min = %d, Max = %d, Mean degree: %1.3f' \
                %(deglist_rand.min(), deglist_rand.max(), deglist_rand.mean()) )
print('Rew \t Min = %d, Max = %d, Mean degree: %1.3f' \
                %(deglist_rew.min(), deglist_rew.max(), deglist_rew.mean()))

# Plot the degree distributions
plt.figure()
plt.hist(deglist_rand, bins=8, range=(0,40), rwidth=0.8, density=True)
plt.xlabel('Node degree', fontsize=14)

plt.figure()
plt.hist(deglist_rew, bins=8, range=(0,40), rwidth=0.8, density=True)
plt.xlabel('Node degree', fontsize=14)

# 2.4) Compare the clustering coefficients
# Calculate clustering of all random and rewired networks
Clist_rand = zeros(nrealiz, float)
Clist_rew = zeros(nrealiz, float)
for re in range(nrealiz):
    Crand, Cnodes_rand = Clustering(randnets[re])
    Clist_rand[re] = Crand
    Crew, Cnodes_rew = Clustering(rewnets[re])
    Clist_rew[re] = Crew

print('\nCompare clustering coefficients ...')
print('Cat:\t%1.3f \nRandom:\t%1.3f \nRew:\t%1.3f' %(C, Crand, Crew))

# 2.5) Compare the pathlengths and the betweenness centralities
# First, calculate pathlengths and BC from the RANDOM graphs
avlenlist_rand = zeros(nrealiz, float)
bclist_rand = []
triuidx = triu_indices(N, k=1)
for re in range(nrealiz):
    Dij_rand, BC_rand, paths, cycles = PathsAllinOne(randnets[re])
    values = Dij_rand[triuidx]
    avlenlist_rand[re] = values.mean()
    bclist_rand += BC_rand.tolist()

# Calculate pathlengths and BC from the REWIRED graphs
avlenlist_rew = zeros(nrealiz, float)
bclist_rew = []
for re in range(nrealiz):
    Dij_rew, BC_rew, paths, cycles = PathsAllinOne(rewnets[re])
    values = Dij_rew[triuidx]
    avlenlist_rew[re] = values.mean()
    bclist_rew += BC_rew.tolist()

# Results for the average pathlength
plt.figure()
plt.title('Comparison of average pathlengths', fontsize=14)
plt.bar( (1,2,3), (avpathlen, avlenlist_rand.mean(), avlenlist_rew.mean()) )
plt.ylim(1,2)
plt.xticks((1,2,3), ('Cat', 'Random', 'Rewired') )
plt.grid(axis='y', lw=0.5)

# Results for the betweenness centrality
plt.figure()
plt.scatter(deg, bcnodes)
plt.plot(deglist_rand, bclist_rand, '.', color='red', zorder=10)
plt.plot(deglist_rew, bclist_rew, '.', color='orange', zorder=5)
plt.xlabel('Node degree', fontsize=14)
plt.ylabel('Betweenness centrality', fontsize=14)
plt.xlim(0,45)
plt.grid(lw=0.5, zorder=100)



# 3) ANALYSIS OF FUNCTIONAL CONNECTOME OF THE CAT ______________________________
## For the cat network we don't have any activity data but for illustration,
## we will just make a theorical approximation to the expected resting-state
## functional connectivity of the cat
# 2.1) Compute theoretical expected functional connectivity
g = 0.0766      # Coupling strength
fcnet = TopologicalSimilarity(netsym, g)

# Visualise the FC matrix
plt.figure()
plt.imshow(fcnet, cmap='jet')
plt.clim(0,1)
plt.colorbar()

# 3.2) Compare SC degree and FC intensity
intens = Intensity(fcnet, directed=False)

plt.figure()
plt.scatter(deg, intens)
plt.xlabel('Node degree (SC)', fontsize=14)
plt.ylabel('Node intensity (FC)', fontsize=14)
plt.grid(lw=0.5)

# 3.3) Compare the graph distance and the pair-wise correlations
distvalues = Dij[triuidx]
fcvalues = fcnet[triuidx]
plt.figure()
plt.plot(distvalues, fcvalues, '.')
plt.xlabel('Graph distance (SC)', fontsize=14)
plt.ylabel('Correlation (FC)', fontsize=14)


# Launch all the plots, ONLY at the end!!
plt.show()

#
